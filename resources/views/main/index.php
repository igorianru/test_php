<?php include_once __DIR__ . '/../layouts/layouts_top.php' ?>

    <div class="panel" style="padding: 10px">
        <form action="/" method="/">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="form-group">
                        <input class="form-control date_time" name="datetime"/>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="width: 100%">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Top 10</div>

        <!-- Table -->
        <table class="table table-responsive">
            <tbody>
            <tr>
                <td>Name</td>
                <td>Name original</td>
                <td>Year</td>
                <td>Rating</td>
                <td>Votes</td>
            </tr>
            </tbody>

            <?php foreach($data['films'] as $val) {?>
                <tr>
                    <td><?=$val['name']?></td>
                    <td><?=$val['name_original']?></td>
                    <td><?=$val['year']?></td>
                    <td><?=$val['rating']?></td>
                    <td><?=$val['votes']?></td>
                </tr>
            <?php } ?>
        </table>
    </div>

<script>
	$('.date_time').daterangepicker({
		locale: {format: 'YYYY-MM-DD'},
		language: 'ru',
		todayHighlight: true,
		singleDatePicker: true,
		timePicker: false,
		timePickerIncrement: 10
	});
</script>

<?php include_once __DIR__ . '/../layouts/layouts_bottom.php' ?>