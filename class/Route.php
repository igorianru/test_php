<?php

namespace Route;

class Route
{
    public function uri($param = null)
    {
        if(isset($_SERVER['SERVER_NAME'])) {
            $parsed_url = parse_url($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);

            $uri['scheme']    = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
            $uri['host']      = isset($parsed_url['host']) ? $parsed_url['host'] : '';
            $uri['port']      = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
            $uri['user']      = isset($parsed_url['user']) ? $parsed_url['user'] : '';
            $uri['pass']      = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
            $uri['pass']      = ($uri['user'] || $uri['pass']) ? $uri['pass'] .'@' : '';
            $uri['path']      = isset($parsed_url['path']) ? $parsed_url['path'] : '';
            $uri['query']     = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
            $uri['fragment']  = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

            return !$param ? $uri : $uri[$param] ?? $uri;
        } else {
            return false;
        }
    }

    /**
     * Reconciliation path route and path uri
     * @param $path
     * @param $route
     * @return bool
     */
    private function _check($path, $route)
    {
        if($path === $this->uri('path') || $path . '/' === $this->uri('path')) {
            $route = explode('@', $route);
            require_once __DIR__.'/../Http/' . $route[0] . '.php';
            $class = $route[0] . '\\' . $route[0];
            $classNew = new $class();
            $route = $route[1];

            return $classNew->$route();
        }

        return false;
    }

    public function get($path, $route)
    {
        return $this->_check($path, $route);
    }

    public function post($path, $route)
    {
        return $this->_check($path, $route);
    }
}
