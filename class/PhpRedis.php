<?php

namespace php_redis;

class phpRedis extends \Redis {
    private $host;
    private $port;

    public function  __construct( $host = '127.0.0.1', $port = 6379)
    {
        $this->host = $host;
        $this->port = $port;

        $this->connect($host, $port);
    }
}