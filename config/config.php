<?php

$path_to_composer = __DIR__. '/../composer.json';

if(file_exists($path_to_composer))
    $composer = json_decode(file_get_contents($path_to_composer), TRUE);
else
    throw new MissingException("composer.json not found");
