<?php
namespace MainController;

use Controller\Controller;

class MainController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Main pages, view top 10
     * @return bool
     */
    public function index()
    {
        $datetime = $_REQUEST['datetime'] ?? '2017-08-08';

        $data['films'] = $this->db->query('SELECT films.*, votes.rating, votes.votes FROM films 
          LEFT JOIN votes ON films.parent_id = votes.parent_id
          WHERE votes.update_at = \'' . $datetime . '\'
          GROUP BY  votes.parent_id, films.id, votes.id  ORDER BY films.id ASC LIMIT 0, 10')->get();

        return $this->render('main/index', $data);
    }

    /**
     * TODO: move in class helpers
     * helper - remove in string ^0-9
     * @param $str
     * @return mixed
     */
    function toNumber($str)
    {
        return  preg_replace('~[^0-9]+~','', $str ?? '');
    }

    /**
     * Parse update ratings Kinopoisk
     */
    public function updateRatings()
    {
        // get html
        $url = 'https://www.kinopoisk.ru/top/';
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        $doc->loadHTMLFile($url);
        $data = [];
        $parent_id = [];

        // create array films & votes
        for($i = 1; 250 >= $i; $i++) {
            $top = $doc->getElementById("top250_place_$i");
            $p = $this->toNumber($top->childNodes[2]->childNodes[0]->attributes[0]->textContent);

            $parent_id[] = $data['films'][$p]['parent_id'] = $data['votes'][$p]['parent_id'] = $p; // id_parent

            $data['films'][$p]['name'] = $top->childNodes[2]->childNodes[0]->textContent; // Стальной гигант (1999)
            $data['films'][$p]['year'] = $this->toNumber($top->childNodes[2]->childNodes[0]->textContent); // 1999
            $data['films'][$p]['name_original'] = $top->childNodes[2]->childNodes[2]->textContent ?? ''; // The Iron Giant
            $data['films'][$p]['create_at'] = date("Y-m-d H:i:s");

            $data['votes'][$p]['position'] = $this->toNumber($top->childNodes[0]->textContent);  // 250.
            $data['votes'][$p]['rating'] = $top->childNodes[4]->childNodes[1]->childNodes[1]->textContent; // 8.046

            $data['votes'][$p]['votes'] = isset($top->childNodes[4]->childNodes[1]->childNodes[3]->textContent)
                ?  $this->toNumber($top->childNodes[4]->childNodes[1]->childNodes[3]->textContent)
                :  $this->toNumber($top->childNodes[4]->childNodes[1]->childNodes[1]->childNodes[2]->textContent); // (51 929)

            $data['votes'][$p]['update_at'] = date("Y-m-d H:i:s");
        }

        // get by parent_id existing records Films
        $result = $this->db->query('SELECT * FROM films WHERE parent_id IN (' . implode(', ', $parent_id) . ')')->get();

        foreach($result as $key => $val)
            unset($data['films'][$val['parent_id']]);

        $votes = $this->db->prepare("INSERT INTO votes (parent_id, rating, votes, update_at, position)
               VALUES (?, ?, ?, ?, ?)");

        // insert Votes
        foreach ($data['votes'] as $val)
            $votes->execute([$val['parent_id'], $val['rating'], $val['votes'], $val['update_at'], $val['position']]);

        $films = $this->db->prepare("INSERT INTO films (parent_id, year, name_original, name, create_at)
               VALUES (?, ?, ?, ?, ?)");

        // insert new Films
        foreach($data['films'] as $val)
            $films->execute([$val['parent_id'], $val['year'], $val['name_original'], $val['name'], $val['create_at']]);
    }
}
