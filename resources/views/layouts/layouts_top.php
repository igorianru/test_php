<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/resources/assets/images/favicon.ico">

    <title>Cover Template for Bootstrap</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="/resources/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- calendar -->
    <link href="/resources/assets/css/calendar/fullcalendar.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/resources/assets/css/style.css" rel="stylesheet">

    <!-- moment  -->
    <script src="/resources/assets/js/moment.js"></script>

    <!-- calendar -->
    <script src="/resources/assets/js/datepicker/daterangepicker.js"></script>
</head>

<body>

<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Test</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Home</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
