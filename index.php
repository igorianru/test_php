<?php

// Require config
require __DIR__.'/config/config.php';

// Require autoload
require __DIR__.'/class/autoload.php';

// Require route
require __DIR__.'/Http/routes.php';
