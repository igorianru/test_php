<?php

namespace DataBase;

require_once __DIR__ . '/Connection.php';
require_once __DIR__ . '/PhpRedis.php';

use Connection\Connection;
use php_redis\phpRedis;

class DataBase extends Connection {
    protected $q;
    protected $r;
    protected $last_query;
    protected $prepare;

    public function __construct()
    {
        parent::__construct('mysql:host=localhost;dbname=test;charset=UTF8;', 'root', '1234');

        $this->r = new phpRedis('localhost', 6379);
    }

    /**
     * querying and caching on redis
     * @param $q
     * @return $this
     */
    public function query($q) {
        if(!$this->r->get($q)) {
            $this->r->set($q, null);
            $this->r->expireAt($q, time() + 3600);
            $this->q = $this->link->query($q, \PDO::FETCH_ASSOC);
        }

        $this->last_query = $q;

        return $this;
    }

    /**
     * get all arrays
     * @return mixed
     */
    public function get()
    {
        if(!$this->r->get($this->last_query)) {
            $this->r->set($this->last_query, json_encode($this->q->fetchAll()) ?? null);
            return $this->q->fetchAll();
        } else
            return json_decode($this->r->get($this->last_query), true);
    }

    /**
     * get first array
     * @return mixed
     */
    public function first() {
        return $this->q->fetch();
    }

    /**
     * pre insert
     * @param $q
     * @return mixed
     */
    public function prepare($q) {
        return $this->link->prepare($q);
    }
}
