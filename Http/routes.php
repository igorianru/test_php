<?php

include __DIR__ . '/../class/Route.php';
$route = new \Route\Route();

$route->get('/', 'MainController@index');
$route->get('/update_ratings', 'MainController@updateRatings');