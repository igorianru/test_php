<?php

namespace Controller;

use DataBase\DataBase;

class Controller {
    public $db;

    public function __construct()
    {
        $this->db = new DataBase();
    }

    /**
     * function views
     * @param null $view
     * @param array $data
     * @return bool
     */
    public function render($view = null, $data = [])
    {
        $view_path = __DIR__. '/../resources/views/' . $view . '.php';

        if(file_exists($view_path))
            require_once $view_path;
        else
            echo 'Not found' . $view;

        return true;
    }
}
